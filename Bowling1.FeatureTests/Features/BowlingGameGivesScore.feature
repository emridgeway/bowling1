﻿
Feature: Bowling Game Gives Score
	As a video game player
	I want to see the final score of a game
	So that I know who won

Scenario: Gutter game scores 0
	Given A new game
	When I roll 20 frames of 0 pins
	Then The score should be 0

Scenario: All 1's scores 20
	Given A new game
	When I roll 20 frames of 1 pins
	Then The score should be 20

Scenario: Spare, then 3, then 0's- scores 16
	Given A new game
	When I roll a spare
	And I roll a frame of 3 pins
	And I roll 17 frames of 0 pins
	Then The score should be 16

Scenario: Strike, then 3, then 4, then 0's- scores 24
	Given A new game
	When I roll a strike
	And I roll a frame of 3 pins
	And I roll a frame of 4 pins
	And I roll 16 frames of 0 pins
	Then The score should be 24

Scenario: Perfect game scores 300
	Given A new game
	When I roll 12 frames of 10 pins
	Then The score should be 300
