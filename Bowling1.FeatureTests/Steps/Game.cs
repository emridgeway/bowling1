﻿namespace Bowling1.FeatureTests.Steps {
    public class Game {
        private int _currentRoll = 0;
        private int[] rollList = new int[21];

        public void roll(int pinsKnockedDown) {
            rollList[_currentRoll++] = pinsKnockedDown;
        }

        public int scoreGame() {
            int score = 0;
            int frameIndex = 0;

            for (int frame = 0; frame < 10; frame++) {
                if (isStrike(frameIndex)) {
                    score += 10 + strikeBonus(frameIndex);
                    frameIndex += 1;
                } else if (isSpare(frameIndex)) {
                    score += 10 + spareBonus(frameIndex);
                    frameIndex += 2;
                } else {
                    score += sumOfBallsInFrame(frameIndex);
                    frameIndex += 2;
                }
            }

            return score;
        }

        private bool isStrike(int frameIndex) {
            return rollList[frameIndex] == 10;
        }

        private int sumOfBallsInFrame(int frameIndex) {
            return rollList[frameIndex] + rollList[frameIndex + 1];
        }

        private int spareBonus(int frameIndex) {
            return rollList[frameIndex + 2];
        }

        private int strikeBonus(int frameIndex) {
            return rollList[frameIndex + 1] + rollList[frameIndex + 2];
        }

        private bool isSpare(int frameIndex) {
            return rollList[frameIndex] + rollList[frameIndex + 1] == 10;
        }
    }
}