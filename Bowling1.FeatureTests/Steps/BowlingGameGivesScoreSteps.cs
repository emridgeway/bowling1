﻿using Should.Fluent;
using TechTalk.SpecFlow;

namespace Bowling1.FeatureTests.Steps {
    [Binding]
    public class BowlingGameGivesScoreSteps {
        private void rollMany(int numberOfFrames, int pinsKnockedDownPerFrame, Game game) {
            for (int i = 0; i < numberOfFrames; i++) {
                game.roll(pinsKnockedDownPerFrame);
            }
        }

        [Given(@"A new game")]
        public void givenANewGame() {
            Game game = new Game();

            ScenarioContext.Current["game"] = game;
        }

        [When(@"I roll (.*) frames of (.*) pins")]
        public void whenIRollFramesOfPins(int numberOfFrames, int pinsKnockedDownPerFrame) {
            Game game = (Game)ScenarioContext.Current["game"];

            rollMany(numberOfFrames, pinsKnockedDownPerFrame, game);
        }

        [When(@"I roll a frame of (.*) pins")]
        public void whenIRollAFrameOfPins(int pinsKnockedDown) {
            Game game = (Game)ScenarioContext.Current["game"];

            game.roll(pinsKnockedDown);
        }

        [When(@"I roll a spare")]
        public void whenIRollASpare() {
            Game game = (Game)ScenarioContext.Current["game"];

            game.roll(4);
            game.roll(6);
        }

        [When(@"I roll a strike")]
        public void whenIRollAStrike() {
            Game game = (Game)ScenarioContext.Current["game"];

            game.roll(10);
        }

        [Then(@"The score should be (.*)")]
        public void thenTheScoreShouldBe(int score) {
            Game game = (Game)ScenarioContext.Current["game"];

            game.scoreGame().Should().Equal(score);
        }
    }
}